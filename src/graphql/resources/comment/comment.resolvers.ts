import { ResolverContext } from './../../../interfaces/ResolverContextInterface';
import { AuthUser } from './../../../interfaces/AuthUserInterface';
import { handleError, throwError } from './../../../utils/utils';
import { CommentInstance } from './../../../models/CommentModel';
import { GraphQLResolveInfo } from 'graphql';

import { DbConnection } from '../../../interfaces/DbConnectionInterface';

import { Transaction } from 'sequelize';
import { compose } from '../../composable/composable.resolver';
import { authResolvers } from '../../composable/auth.resolver';
import { DataLoaders } from '../../../interfaces/DataLoadersInterface';

export const commentResolvers = {


    Comment: {

        user: (parent, args, {db, dataloaders: {userLoader}}: {db: DbConnection, dataloaders: DataLoaders}, info: GraphQLResolveInfo) => {
            return userLoader
                .load({key: parent.get('user'), info})
                .catch(handleError);
        },

        post: (parent, args, {db, dataloaders: {postLoader}}: {db: DbConnection, dataloaders: DataLoaders}, info: GraphQLResolveInfo) => {
            return postLoader
                .load({key: parent.get('post'), info})
                .catch(handleError);
        }

    },

    Query: {

        commentsByPost: compose() ((parent, {postId, first = 10, offset = 0}, context: ResolverContext, info: GraphQLResolveInfo) => {
            postId = parseInt(postId);
            return context.db.Comment
                .findAll({
                    where: {post: postId},
                    limit: first,
                    offset: offset,
                    attributes: context.requestedFields.getFields(info, {keep: undefined})
                })
                .catch(handleError);
        })

    },

    Mutation: {

        createComment: compose(...authResolvers) ((parent, {input}, {db, authUser}: {db: DbConnection, authUser: AuthUser}, info: GraphQLResolveInfo) => {
            input.user = authUser.id;
            return db.sequelize.transaction((t: Transaction) => {
                return db.Comment
                    .create(input, {transaction: t});
            }).catch(handleError);  
        }),

        updateComment: compose(...authResolvers) ((parent, {id, input}, {db, authUser}: {db: DbConnection, authUser: AuthUser}, info: GraphQLResolveInfo) => {
            id = parseInt(id);
            return db.sequelize.transaction((t: Transaction) => {
                return db.Comment
                    .findById(id)
                    .then((comment: CommentInstance) => {
                        throwError(!comment, `Comment with id ${id} not found`);
                        throwError(comment.get('user') != authUser.id, `Unauthorized! You can onli edit comments by yourself!`)
                        input.user = authUser.id;
                        return comment.update(input, {transaction: t});
                    });
            }).catch(handleError);   
        }),

        deleteComment: compose(...authResolvers) ((parent, {id}, {db, authUser}: {db: DbConnection, authUser: AuthUser}, info: GraphQLResolveInfo) => {
            id = parseInt(id);
            return db.sequelize.transaction((t: Transaction) => {
                return db.Comment
                    .findById(id)
                    .then((comment: CommentInstance) => {
                        throwError(!comment, `Comment with id ${id} not found`);
                        throwError(comment.get('user') != authUser.id, `Unauthorized! You can onli delete comments by yourself!`)
                        return comment.destroy({transaction: t})
                            .then(comment => !!comment);
                    });
            }).catch(handleError); 
        })
    }
};